#!/bin/sh
set -ex

rootdir=$(dirname $0)
template=$rootdir/mission_document-standalone.html.j2

export CI_COMMIT_SHORT_SHA=`git log --pretty="%h" -1`

for f in $rootdir/*.yaml; do
    echo $f
    export NOW="`date`"
    j2 $template $f -o ${f%%.yaml}.html
    weasyprint ${f%%.yaml}.html ${f%%.yaml}.pdf
done

mv $rootdir/*.pdf ./pdf