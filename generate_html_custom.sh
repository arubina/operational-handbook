set -ex

# Create Deliverable List (URL documents)
python3 makeDelList/main.py
mv makeDelList/deliverables_list.md docs

# Create md files from yaml files
bash docs/mission_documents/build_md.sh

# Merge technical committee WG files under the technical committee one
tc_files=docs/mission_documents/technical-committee_*.md
tc=docs/mission_documents/technical-committee.md

# Create new Technical Committe md where we merge all other tc md, including th tc one. The goal is to have all WG and the TC specs cleanly displayed in the navigation bar
echo "# Technical Committee" > docs/mission_documents/Technical_Committee.md

cat $tc >> docs/mission_documents/Technical_Committee.md

for file in ${tc_files[@]}; do
  echo $file
  cat $file >> docs/mission_documents/Technical_Committee.md
done

# Move all remaining md files into docs
mv docs/mission_documents/*.md docs

# Remove all tc wg md files and tc md from docs
rm docs/technical-committee.md
md_files=docs/technical-committee_*.md
for file in ${md_files[@]}; do
  rm $file
done


